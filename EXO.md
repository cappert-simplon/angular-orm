# Angular ORM (projet front en lien avec Spring ORM)

## Affichage d'une fiche chien
1. Générer un nouveau component SingleDog, lui assigner une route paramétrée `/dog/:id` et dedans faire en sorte de récupérer le chien, comme on a déjà fait plusieurs fois (avec le ActivatedRoute, le switchMap, rajouter une méthode getOne dans le DogService et tout)
2. Côté DogList, modifier les li pour rajouter un \<a> dedans avec un routerLink qui pointe sur le chien
3. Faire l'affichage du chien dans le single-dog.component.html, en mettant ses informations ainsi que celles de son trainer, vu qu'on les a sous la main
4. Côté spring, créer une nouvelle route en get sur `/api/dog/{id}/contest` qui va renvoyer une liste de de Contest (pour ça, on récupère l'id du chien, on fait un getOne avec et on renvoie son getContests)
5. Côté angular, rajouter une méthode getContests dans le DogService qui va pointer sur cette nouvelle route, et faire en sorte de l'appeler dans le SingleDogComponent de la même manière que la méthode getOne
6. Générer un component ContestList qui attendra en input un tableau de Contest, et dedans faire l'affichage de la liste, sous le format souhaité avec un ngFor
7. Dans le template du SingleDog, afficher le app-contest-list en lui donnant à manger la liste de contests
   (ptite parenthèse, mais rajouter ça au dessus de la propriété birthdate de l'entité Dog côté java :     @JsonFormat(pattern = "yyyy-MM-dd"))

## Inscrire un chien à un concours 
(pour ça, on aura besoin de la liste des concours et de faire appel à la route `/api/dog/{id}/contest/{id}`)
1. Générer un service pour les contest et dedans créer une route getAll qui va faire un appel vers /api/contest (et renvoyer un tableau de contest)
2. Générer un component ChooseContest dans lequel on injecte le ContestService et on lance son getAll dans le ngOnInit qu'on assigne à une propriété contests
3. On fait le template pour faire en sorte d'afficher un select avec la liste des contests en option (avec un ngfor) qu'on lie à une propriété avec un ngModel, et on rajoute un bouton Ok
4. Dans le ChooseContestComponent, rajouter une propriété avec un @Output qu'on va appeler choose et qui sera de type EventEmitter<Contest>, puis créer une méthode submit() qui va déclencher un emit sur le choose en lui donnant en argument le contest
5. Dans le DogService rajouter une méthode applyToContest qui va attendre 2 arguments, un idDog et un idContest et déclencher un patch vers /api/dog/idDog/contetst/idContest
6. Dans le SingleDogComponent, créer une méthode apply qui va attendre un contest en argument et qui va faire un subscribe sur le applyToContest
7. Afficher le app-choose-contest dans le template du single dog et sur son event (choose) assigner la méthode apply 

### Choper le chien et ses concours avec un seul subscribe ¯\\_(ツ)_/¯
```typescript
this.route.params.pipe(
  switchMap(params => forkJoin({
    dog: this.dogService.getOne(params['id']),
    contests: this.dogService.getContest(params['id'])
  }))
  ).subscribe(data => {
    this.dog = data.dog;
    this.contests = data.contests;
  });
```

~JD
