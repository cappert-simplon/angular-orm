import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseContestComponent } from './choose-contest.component';

describe('ChooseContestComponent', () => {
  let component: ChooseContestComponent;
  let fixture: ComponentFixture<ChooseContestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChooseContestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseContestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
