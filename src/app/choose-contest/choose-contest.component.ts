import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Contest} from "../entities";

@Component({
  selector: 'app-choose-contest',
  templateUrl: './choose-contest.component.html',
  styleUrls: ['./choose-contest.component.css']
})
export class ChooseContestComponent implements OnInit {

  constructor() {
  }

  @Output
  choose: EventEmitter<Contest>;

  ngOnInit(): void {
  }

  // TODO
  submit(): void {
    this.choose.emit(contest)
    //   this.fetchContests();
  }
  // TODO paginer les contests (bonus)
  nextPage() {
    this.page++;
    this.fetchContests();
  }

  previousPage() {
    this.page--;
    this.fetchContests();
  }

  fetchContests() {
    this.contestService.getAll(this.page, this.pageSize).subscribe(data => this.contests = data);
  }
}

