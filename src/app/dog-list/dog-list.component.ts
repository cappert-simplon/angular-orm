import { Component, OnInit } from '@angular/core';
import { DogService } from '../dog.service';
import { Dog, Page } from '../entities';

@Component({
  selector: 'app-dog-list',
  templateUrl: './dog-list.component.html',
  styleUrls: ['./dog-list.component.css']
})
export class DogListComponent implements OnInit {
  dogs?: Page<Dog>;
  page = 0;
  pageSize = 10;

  constructor(private dogService: DogService) { }

  ngOnInit(): void {
    this.fetchDogs();
  }

  nextPage() {
    this.page++;
    this.fetchDogs();
  }
  previousPage() {
    this.page--;
    this.fetchDogs();
  }

  fetchDogs() {
    this.dogService.getAll(this.page, this.pageSize).subscribe(data => this.dogs = data);
  }
}
