export interface Dog {
  id?: number;
  name: string;
  breed: string;
  birthdate: string;
  trainer: Trainer;
  contest: Contest[];
}

export interface Trainer {
  id?: number;
  name: string;
  city: string;
  dogs: Dog[];
}

export interface Contest {
  id?: number;
  date: string;
  type: string;
  dogs: Dog[];
}

/**
 * Ici on fait une interface Page générique qui reprend
 * les propriétés des Page<T> en Java ce qui nous permettra
 * de récupérer les informations de paginations
 * (Le générique signifie que cette interface pourra servir pour
 * n'importe quel autre type, exactement comme on peut faire une List<Dog>
 * ou une List<Person> en java, ici on pourra faire un Page<Dog> ou un Page<Person>)
 */
export interface Page<T> {
  content: T[];

  totalPages: number;
  totalElements: number;
  last: boolean;
  first: boolean;

  size: number;
  number: number;
  numberOfElements: number;
  empty: boolean;
}
