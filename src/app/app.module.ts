import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SingleDogComponent } from './single-dog/single-dog.component';
import { DogListComponent } from './dog-list/dog-list.component';
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import { ChooseContestComponent } from './choose-contest/choose-contest.component';

@NgModule({
  declarations: [
    AppComponent,
    SingleDogComponent,
    DogListComponent,
    ChooseContestComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
